class Graph:

    _graph = {}
    _cost = {}
    
    def createGraph(self, csv_file_path):
        self._graph = {}
        self._cost = {}

        try:
            csvFile = open(csv_file_path, "r")
        except:
            return False

        next_line = csvFile.readline()
        while next_line != "":
            values = next_line.rstrip().split(",")

            if values[0] in self._graph:
                if values[1] not in self._graph[values[0]]:
                    if (values[1].rstrip('0') != ''):
                        self._graph[values[0]].append(values[1])
            else:
                if (values[1].rstrip('0') != ''):
                        self._graph[values[0]] = [values[1]]

            if values[1] in self._graph:
                if values[0] not in self._graph[values[1]]:
                    if (values[0].rstrip('0') != ''):
                        self._graph[values[1]].append(values[0])
            else:
                if (values[0].rstrip('0') != ''):
                        self._graph[values[1]] = [values[0]]
            
            self._cost[values[0] + values[1]] = int(values[2])
            self._cost[values[1] + values[0]] = int(values[2])

            next_line = csvFile.readline()
            
        csvFile.close()
        return True
