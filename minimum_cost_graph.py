from graph import Graph

class MinimumCostGraph(Graph):

    def pathMinimumCost(self, start_node, goal_node, path = [], cost = 0):
        if start_node not in self._graph or goal_node not in self._graph:
            return [[], cost, False]
            
        if start_node is goal_node:
            path.append(start_node)
            path.append(goal_node)
            return [path, 0, True]

        if len(self._graph[start_node]) == 0:
            path.append(start_node)
            return [[], cost, False]

        if goal_node in self._graph[start_node]:
            path.append(start_node)
            path.append(goal_node)
            return [path, cost + self._cost[start_node + goal_node], True]
        
        
        results = []
        for node in self._graph[start_node]:
            if node not in path:
                next_path = [p for p in path]
                next_path.append(start_node)
                results.append(self.pathMinimumCost(node, goal_node, next_path, cost + self._cost[start_node + node]))

        if len(results) > 0:
            results_finish = [result for result in results if result[2]]
            if len(results_finish) > 0:
                min_result = results_finish[0] 
                for result in results_finish: 
                    if result[1] < min_result[1]:  
                        min_result = result

                return min_result
            else:
                return [[], cost, False]
        else:
            return [[], cost, False]
