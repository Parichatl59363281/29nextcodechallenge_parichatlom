import unittest
from minimum_cost_graph import MinimumCostGraph

class TestMininumCostGraph(unittest.TestCase):
    
    # Senario: 1
    # What is start node?: A
    # What is goal node?: A
    # Path from A to A is A->A, and have cost 0.

    def testPathAToA(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('A', 'A', [], 0)
        self.assertEqual(result, [['A', 'A'], 0, True])
    
    # Senario: 2
    # What is start node?: A
    # What is goal node?: B
    # Path from A to B is A->B, and have cost 5.
    
    def testPathAToB(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('A', 'B', [], 0)
        self.assertEqual(result, [['A', 'B'], 5, True])

    # Senario: 3
    # What is start node?: B
    # What is goal node?: A
    # Path from B to A is B->A, and have cost 5.

    def testPathBToA(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('B', 'A', [], 0)
        self.assertEqual(result, [['B', 'A'], 5, True])

    # Senario: 4
    # What is start node?: A
    # What is goal node?: 0
    # Path from A to 0 , It should be impossible to find a route because there is no goal node."

    def testPathATo0(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('A', '0', [], 0)
        self.assertEqual(result, [[], 0, False])

    # Senario: 5
    # What is start node?: 0
    # What is goal node?: A
    # Path from 0 to A , It should be impossible to find a route because there is no start node."

    def testPath0ToA(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('0', 'A', [], 0)
        self.assertEqual(result, [[], 0, False])

    # Senario: 6
    # What is start node?: C
    # What is goal node?: F
    # Path from C to F is C->G->H->F, and have cost 10.

    def testPathCToF(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('C', 'F', [], 0)
        self.assertEqual(result, [['C', 'G', 'H', 'F'], 10, True])
        
    # Senario: 7
    # What is start node?: F
    # What is goal node?: C
    # Path from F to C is F->H->G->C, and have cost 10.

    def testPathFToC(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('F', 'C', [], 0)
        self.assertEqual(result, [['F', 'H', 'G', 'C'], 10, True])

    # Senario: 8
    # What is start node?: F
    # What is goal node?: G
    # Path from F to G is F->H->G, and have cost 8.

    def testPathFToG(self):
        graph = MinimumCostGraph()
        graph.createGraph('graph.csv')
        result = graph.pathMinimumCost('F', 'G', [], 0)
        self.assertEqual(result, [['F', 'H', 'G'], 8, True])

    


