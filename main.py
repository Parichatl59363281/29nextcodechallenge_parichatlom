from minimum_cost_graph import MinimumCostGraph
import os

graph = MinimumCostGraph()

file_name = input("What is graph file name: ")

while not graph.createGraph(file_name):
    print("Could not read file, please try again.")
    file_name = input("What is graph file name: ")

exit = False

while not exit:
    start_node = input("What is start node: ")
    goal_node = input("What is goal node: ")

    [path, cost, finish] = graph.pathMinimumCost(start_node, goal_node, [], 0)

    if finish:
        print("Path from " + start_node + " to "+ goal_node +" is " + "->".join(path) +", and have cost " + str(cost) + ".")
    else:
        print("The path cannot be found because there is no start node or goal node.")

    keypass = input("Would you like to try again? [yes / no] ? ")
    if keypass.lower() != 'yes' and keypass.lower() != 'y':
        exit = True
